package com.example.primeraversion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private CardView cardView1, cardView2, cardView3, cardView4, cardView5, cardView6;
    private Animation smalltobig, btta, btta2, btta3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().setNavigationBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_main);

        cornerRadius();

        smalltobig = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(this, R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(this, R.anim.btta2);
        btta3 = AnimationUtils.loadAnimation(this, R.anim.btta3);

        cardView1 = findViewById(R.id.rss_1);
        cardView2 = findViewById(R.id.rss_2);
        cardView3 = findViewById(R.id.rss_3);
        cardView4 = findViewById(R.id.rss_4);
        cardView5 = findViewById(R.id.rss_5);
        cardView6 = findViewById(R.id.rss_6);

        cardView1.startAnimation(btta3);
        cardView2.startAnimation(btta3);
        cardView3.startAnimation(btta3);
        cardView4.startAnimation(btta3);
        cardView5.startAnimation(btta3);
        cardView6.startAnimation(btta3);

    }

    public void cornerRadius(){

        Drawable originalDrawable = getResources().getDrawable(R.drawable.desayuno);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();
        RoundedBitmapDrawable roundedDrawable =
                RoundedBitmapDrawableFactory.create(getResources(), originalBitmap);
        roundedDrawable.setCornerRadius(originalBitmap.getHeight());
        ImageView imageView = (ImageView) findViewById(R.id.imgDesayuno);
        imageView.setImageDrawable(roundedDrawable);


        Drawable original2Drawable = getResources().getDrawable(R.drawable.almuerzos);
        Bitmap original2Bitmap = ((BitmapDrawable) original2Drawable).getBitmap();
        RoundedBitmapDrawable rounded2Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original2Bitmap);
        rounded2Drawable.setCornerRadius(original2Bitmap.getHeight());
        ImageView imageView2 = (ImageView) findViewById(R.id.imgAlmuerzo);
        imageView2.setImageDrawable(rounded2Drawable);


        Drawable original3Drawable = getResources().getDrawable(R.drawable.meriendas);
        Bitmap original3Bitmap = ((BitmapDrawable) original3Drawable).getBitmap();
        RoundedBitmapDrawable rounded3Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original3Bitmap);
        rounded3Drawable.setCornerRadius(original3Bitmap.getHeight());
        ImageView imageView3 = (ImageView) findViewById(R.id.imgMerienda);
        imageView3.setImageDrawable(rounded3Drawable);


        Drawable original4Drawable = getResources().getDrawable(R.drawable.bebidas);
        Bitmap original4Bitmap = ((BitmapDrawable) original4Drawable).getBitmap();
        RoundedBitmapDrawable rounded4Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original4Bitmap);
        rounded4Drawable.setCornerRadius(original4Bitmap.getHeight());
        ImageView imageView4 = (ImageView) findViewById(R.id.imgBebida);
        imageView4.setImageDrawable(rounded4Drawable);


        Drawable original5Drawable = getResources().getDrawable(R.drawable.promo);
        Bitmap original5Bitmap = ((BitmapDrawable) original5Drawable).getBitmap();
        RoundedBitmapDrawable rounded5Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original5Bitmap);
        rounded5Drawable.setCornerRadius(original5Bitmap.getHeight());
        ImageView imageView5 = (ImageView) findViewById(R.id.imgPromo);
        imageView5.setImageDrawable(rounded5Drawable);


        Drawable original6Drawable = getResources().getDrawable(R.drawable.anvorguesa);
        Bitmap original6Bitmap = ((BitmapDrawable) original6Drawable).getBitmap();
        RoundedBitmapDrawable rounded6Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original6Bitmap);
        rounded6Drawable.setCornerRadius(original6Bitmap.getHeight());
        ImageView imageView6 = (ImageView) findViewById(R.id.imgAnvorguiesa);
        imageView6.setImageDrawable(rounded6Drawable);

        Drawable original7Drawable = getResources().getDrawable(R.drawable.xxx);
        Bitmap original7Bitmap = ((BitmapDrawable) original7Drawable).getBitmap();
        RoundedBitmapDrawable rounded7Drawable =
                RoundedBitmapDrawableFactory.create(getResources(), original7Bitmap);
        rounded7Drawable.setCornerRadius(original7Bitmap.getHeight());
        ImageView imageView7 = (ImageView) findViewById(R.id.xxx);
        imageView7.setImageDrawable(rounded7Drawable);
    }
}
